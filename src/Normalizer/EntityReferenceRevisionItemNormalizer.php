<?php

namespace Drupal\entity_pilot\Normalizer;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_reference_revisions\Normalizer\EntityReferenceRevisionItemNormalizer as BaseEntityReferenceRevisionItemNormalizer;
use Drupal\hal\LinkManager\LinkManagerInterface;
use Drupal\serialization\EntityResolver\EntityResolverInterface;

/**
 * Defines a class for normalizing entity reference revision items.
 */
class EntityReferenceRevisionItemNormalizer extends BaseEntityReferenceRevisionItemNormalizer {

  /**
   * Parent normalizer.
   *
   * @var \Drupal\entity_reference_revisions\Normalizer\EntityReferenceRevisionItemNormalizer
   */
  private $parentNormalizer;

  /**
   * Constructs a new EntityReferenceRevisionItemNormalizer.
   *
   * @param \Drupal\hal\LinkManager\LinkManagerInterface $link_manager
   *   Link manager.
   * @param \Drupal\serialization\EntityResolver\EntityResolverInterface $entity_resolver
   *   Resolver.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Manager.
   * @param \Drupal\entity_reference_revisions\Normalizer\EntityReferenceRevisionItemNormalizer $parentNormalizer
   *   Parent.
   */
  public function __construct(LinkManagerInterface $link_manager, EntityResolverInterface $entity_resolver, EntityTypeManagerInterface $entity_type_manager, BaseEntityReferenceRevisionItemNormalizer $parentNormalizer) {
    parent::__construct($link_manager, $entity_resolver, $entity_type_manager);
    $this->parentNormalizer = $parentNormalizer;
  }

  /**
   * {@inheritdoc}
   */
  protected function constructValue($data, $context) {
    $field_item = $context['target_instance'];
    $field_definition = $field_item->getFieldDefinition();
    $target_type = $field_definition->getSetting('target_type');
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    if ($entity = $this->entityResolver->resolve($this, $data, $target_type)) {
      // The exists plugin manager may nominate an existing entity to use here.
      if (($id = $entity->id()) && ($revision_key = $entity->getEntityType()->getKey('revision')) && ($revision_id = $entity->get($revision_key)->value)) {
        return ['target_id' => $id, 'target_revision_id' => $revision_id];
      }
      return ['entity' => $entity];
    }
    // We didn't find anything, so defer to the parent normalizer.
    return $this->parentNormalizer->constructValue($data, $context);
  }

}
