<?php

namespace Drupal\Tests\entity_pilot\Kernel;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\entity_pilot\Entity\Account;
use Drupal\entity_pilot\Entity\Arrival;
use Drupal\entity_pilot\FlightInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Defines a class for testing paragraphs.
 *
 * @group entity_pilot
 * @requires module paragraphs
 */
class ParagraphsImportTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_pilot',
    'serialization',
    'rest',
    'hal',
    'node',
    'user',
    'text',
    'system',
    'paragraphs',
    'entity_reference_revisions',
    'filter',
    'options',
    'field',
    'file',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->installEntitySchema('node');
    $this->installEntitySchema('paragraph');
    $this->installSchema('system', ['sequences']);
    $this->installEntitySchema('user');
    $this->installEntitySchema('ep_arrival');
    $this->installConfig(['filter', 'node']);
  }

  /**
   * Tests that paragraphs import properly.
   */
  public function testParagraphImport() {
    $paragraph_type = ParagraphsType::create([
      'id' => $this->randomMachineName(),
      'label' => $this->randomMachineName(),
    ]);
    $paragraph_type->save();
    $node_type = NodeType::create([
      'type' => $this->randomMachineName(),
      'name' => $this->randomMachineName(),
    ]);
    $node_type->save();
    $field_name = 'components';
    $field = FieldStorageConfig::create([
      'field_name' => $field_name,
      'type' => 'entity_reference_revisions',
      'entity_type' => 'node',
      'settings' => ['target_type' => 'paragraph'],
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
    ]);
    $field->save();
    $instance = FieldConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'node',
      'bundle' => $node_type->id(),
      'label' => $this->randomMachineName(),
      'settings' => [
        'handler' => 'default:paragraph',
        'handler_Settings' => [
          'negate' => FALSE,
          'target_bundles' => [
            $paragraph_type->id() => $paragraph_type->id(),
          ],
          'target_bundles_drag_drop' => [
            $paragraph_type->id() => [
              'enabled' => TRUE,
              'weight' => 0,
            ],
          ],
        ],
      ],
    ]);
    $instance->save();
    $paragraph_field_name = 'content';
    $paragraph_field = FieldStorageConfig::create([
      'field_name' => $paragraph_field_name,
      'type' => 'string',
      'entity_type' => 'paragraph',
    ]);
    $paragraph_field->save();
    $paragraph_instance = FieldConfig::create([
      'field_name' => $paragraph_field_name,
      'entity_type' => 'paragraph',
      'bundle' => $paragraph_type->id(),
      'label' => $this->randomMachineName(),
    ]);
    $paragraph_instance->save();
    $values = $paragraphs = [];
    foreach (range(0, 3) as $delta) {
      $values[] = $this->randomMachineName(32);
      $paragraphs[] = Paragraph::create([
        'status' => true,
        'type' => $paragraph_type->id(),
        $paragraph_field_name => end($values),
      ]);
    }
    $node = Node::create([
      'type' => $node_type->id(),
      'title' => $this->randomMachineName(),
      $field_name => $paragraphs,
      'status' => true,
      'uid' => $this->createUser([]),
    ]);
    $account = Account::create([
      'id' => $this->randomMachineName(),
      'label' => $this->randomMachineName(),
    ]);
    $account->save();
    $serializer = \Drupal::service('serializer');
    $author = $node->uid->entity;
    $contents = [
      $author->uuid() => $serializer->normalize($author, 'hal_json'),
      $node->uuid() => $serializer->normalize($node, 'hal_json'),
    ];
    $approved_uuids = [$node->uuid()];
    foreach ($paragraphs as $paragraph) {
      $approved_uuids[] = $paragraph->uuid();
      $contents[$paragraph->uuid()] = $serializer->normalize($paragraph, 'hal_json');
    }
    $arrival = Arrival::create([
      'account' => $account->id(),
      'status' => FlightInterface::STATUS_QUEUED,
      'info' => $this->randomMachineName(),
      'remote_id' => 1,
      'contents' => Json::encode($contents),
      'approved_passengers' => $approved_uuids,
    ]);
    $arrival->save();
    $customs = \Drupal::service('entity_pilot.customs');
    $customs->screen($arrival);
    $saved = $customs->approve($arrival);
    $this->assertCount(5, $saved);
    foreach ($saved as $entity) {
      $this->assertNotEmpty($entity->label());
    }
    $nodes = array_filter($saved, function (EntityInterface $entity) {
      return $entity->getEntityTypeId() === 'node';
    });
    /** @var \Drupal\node\NodeInterface $node */
    $imported_node = reset($nodes);
    $imported_references = $paragraph_contents = [];
    foreach ($imported_node->components as $component) {
      $imported_references[] = $component->entity->id();
      $paragraph_contents[] = $component->entity->content->value;
    }
    $this->assertCount(4, $imported_references);
    $imported_paragraphs = array_filter($saved, function (EntityInterface $entity) {
      return $entity->getEntityTypeId() === 'paragraph';
    });
    $paragraph_ids = array_values(array_map(function (Paragraph $paragraph) {
      return $paragraph->id();
    }, $imported_paragraphs));
    $this->assertEquals($paragraph_ids, $imported_references);
    $this->assertEquals($values, $paragraph_contents);
  }

}
